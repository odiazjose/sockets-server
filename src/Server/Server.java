package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.*;
import static java.lang.System.exit;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Server{
    private int port;
    private ServerSocket serverS;
    private Socket clientS;
    private Pattern validCommandPattern = Pattern.compile("@@-(\\d+)(/@@(.*?)@@&)*-@@", Pattern.DOTALL);
    private Pattern parameterPattern = Pattern.compile("(/@@)(.*?)(@@&)", Pattern.DOTALL);
    private Matcher commandCodeFinder, paramFinder;
    private final String directory = "C:\\Tests\\SocketProject\\";
    
    public Server(int portNumber){
        port = portNumber;
        try {
            serverS = new ServerSocket(port);
        } catch (IOException ex) {
            System.out.print(ex);
            exit(-1);
        }
    }
    
    public void listen(){
        try{
            System.out.println("Server is listening");
            clientS = serverS.accept();

            BufferedReader br = new BufferedReader(new InputStreamReader(clientS.getInputStream()));
            String cmd = "";
            String line;
            while((line = br.readLine()) != null){
                cmd += line;
                if(line.endsWith("-@@")) break;
                else cmd += System.lineSeparator();
            }
            System.out.println("Command received");
            
            response(executeCommand(cmd));
            br.close();
        } catch (IOException ex) {
          System.out.print("An exception has ocurred while receiving a message from the client. Exception: " + ex);
          exit(-1);
        }
    }
    
    public void response(String msg){
        PrintWriter wr;
        try {
            wr = new PrintWriter(clientS.getOutputStream(), true);
            wr.write(msg + System.lineSeparator());
            System.out.println("Response dispatched");

            wr.flush();
            wr.close();
        } catch (IOException ex) {
            System.out.print("An exception has ocurred while sending a response to the client. Exception: " + ex);
            exit(-1);
        }
    }
    
    public String executeCommand(String cmd){
        String execMsg = null;
        ArrayList parameters = new ArrayList();
        commandCodeFinder = validCommandPattern.matcher(cmd);
        
        if(commandCodeFinder.matches()){
            paramFinder = parameterPattern.matcher(cmd);
            while(paramFinder.find()){ parameters.add(paramFinder.group(2)); 
            //System.out.println("Parameter line: " + paramFinder.group(2));
            }
            String cmdCode = commandCodeFinder.group(1);
            System.out.println("Attempting to execute command " + cmdCode);
            switch(cmdCode){
                default:
                    execMsg = "Valid command structure, but command number "
                            + "could not be matched to available operations.";
                    break;
                //Get file names
                case "000":
                    execMsg = fileNames();
                    break;
                //Open file
                case "001":
                    execMsg = openFile((String) parameters.get(0));
                    break;
                //Edit file
                case "002": 
                    execMsg = editFile((String) parameters.get(0), (String) parameters.get(1));
                    break;
                //Write file
                case "003":
                    execMsg = writeFile((String) parameters.get(0), (String) parameters.get(1));
                    break;
                //Write as binary
                case "004":
                    execMsg = writeAsBin((String) parameters.get(0), (String) parameters.get(1));
                    break;
                //Read as binary
                case "005":
                    execMsg = readAsBin((String) parameters.get(0));
                    break;
                //Delete file
                case "999":
                    execMsg = deleteFile((String) parameters.get(0));
                    break;
            }
        } else execMsg = "Invalid command structure";
        
        return execMsg;
    }
    
    private String fileNames(){
        String response = "";
        
        File[] files = new File(directory).listFiles();
        for(int i = 0; i < files.length; i++){
            response += files[i].getName();
            if(i < files.length - 1) response += ", ";
        }
        
        return response;
    }
    
    private String openFile(String fileName){
        Path path = Paths.get(directory + fileName);
        String content = "";
        
        if(Files.exists(path)){
            File F = new File(path.toString());
            try{
                Scanner fileScanner = new Scanner(F);
                
                while(fileScanner.hasNextLine()){
                    content += fileScanner.nextLine(); 
                    if(fileScanner.hasNextLine()) content += System.lineSeparator();
                }
                
                fileScanner.close();
            }catch(IOException ex){
                System.out.print("An exception has ocurred while reading a file. Exception: " + ex);
                content = "File couldn't be read. Exception: " + ex;
            }
        }
        else try {
            Files.createFile(path);
        } catch (IOException ex) {
            System.out.print("An exception has ocurred while creating a file. Exception: " + ex);
            content = "File couldn't be created. Exception: " + ex;
        }
        
        return content;
    }
    
    private String editFile(String fileName, String content){
        Path path = Paths.get("C:\\Tests\\SocketProject\\" + fileName);
        
        if(Files.exists(path)){
            File F = new File(path.toString());
            try{
                FileWriter FW = new FileWriter(F);
                FW.write(content);
                FW.flush();
                FW.close();
                return "File edited successfully";
            }catch(IOException ex){
                System.out.print("An exception has ocurred while writing a file. Exception: " + ex);
                return "An exception has ocurred while writing a file. Exception: " + ex;
            }
        }else{
            return "File \"" + fileName + "\" could not be found.";
        }
    }
    
    private String deleteFile(String fileName){
        Path path = Paths.get(directory + fileName);
        
        if(Files.exists(path)){
            try{
                Files.delete(path);
                return "File deleted successfully";
            }catch(IOException ex){
                System.out.print("An exception has ocurred while deleting a file. Exception: " + ex);
                return "An exception has ocurred while writing a file. Exception: " + ex;
            }
        }else{
            return "File \"" + fileName + "\" could not be found.";
        }
    }
    
    private String writeFile(String fileName, String dataString){
        Path path = Paths.get(directory + fileName);
        String content = "";
        
        if(Files.exists(path)){
            try { 
                FileOutputStream os = new FileOutputStream(path.toString());
                try {
                    String[] decodedData = dataString.split(",");
                    byte[] data = new byte[decodedData.length];

                    for(int i = 0; i < decodedData.length; i++){
                        data[i] = Byte.parseByte(decodedData[i]);
                    }
                    
                    System.out.println("Writing file...");
                    os.write(data);
                    os.flush();
                    os.close();
                } catch (IOException ex) {
                    System.out.print("An exception has ocurred while writing a file. Exception: " + ex);
                    content = "File couldn't be writen to. Exception: " + ex;
                }
            } catch (FileNotFoundException ex) {
                System.out.print("An exception has ocurred while searching a file. Exception: " + ex);
                content = "File couldn't be found. Exception: " + ex;
            }
        }
        else try {
            Files.createFile(path);
            writeFile(fileName, dataString);
        } catch (IOException ex) {
            System.out.print("An exception has ocurred while creating a file. Exception: " + ex);
            content = "File couldn't be created. Exception: " + ex;
        }
        
        return content;
    }
    
    private String writeAsBin(String fileName, String encodedData){
        return writeFile(fileName + ".bin", encodedData);
    }
    
    private String readAsBin(String fileName){
        Path path = Paths.get(directory + fileName);
        String content = "";
        
        if(Files.exists(path)){
            try {
                content = Arrays.toString(Files.readAllBytes(path));
            } catch (IOException ex) {
                System.out.print("An exception has ocurred while reading a file as binary. Exception: " + ex);
                content = "File could not be read. Exception: " + ex;
            }
        }
        
        return content;
    }
}